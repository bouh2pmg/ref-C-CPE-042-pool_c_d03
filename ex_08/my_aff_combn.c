#include <stdio.h>
#include <unistd.h>

void my_putchar(char c)
{
  write(1, &c, 1);
}

int getMax(int n)
{
  int max;
  return max;
}

int isOkToPrint(int i, int digit, int last_digit, int n)
{
  int current_digit;

  current_digit = i % 10;
  if (digit == n && last_digit > current_digit || digit + 1 == n && i == 0 && last_digit > 0)
    return 1;
  else if (i == 0)
    return 0;
  if (current_digit < last_digit)
    return isOkToPrint(i / 10, digit + 1, current_digit, n);
   return 0;
}

int my_putnbr(int nb)
{
  int n;

  if (nb < 0 && nb > -10)
    {
      nb *= -1;
      write(1, "-", 1);
    }
  n = nb;
  if (nb > 9 || nb < -9)
    nb = nb - my_putnbr(nb / 10) * 10;
  my_putchar(nb + '0');
  return n;
}

void printNb(int nb, int n, int max)
{
  int i;
  int tmp;

  i = 1;
  tmp = nb;
  while (tmp > 10)
    {
      tmp /= 10;
      ++i;
    }
  if (i < n)
    my_putchar('0');
  my_putnbr(nb);
  if (nb < max)
    {
      my_putchar(',');
      my_putchar(' ');
    }
}

int my_aff_combn(int n)
{
  int i;
  int max;
  int tmp;

  max = 0;
  i = 10 - n;
  tmp = n;
  while (n-- > 0)
      max = max * 10 + i++;
  i = 0;
  n = tmp;
  while (i <= max)
    {
      if (isOkToPrint(i / 10, 1, i % 10, n))
	printNb(i, n, max);
      ++i;
    }
  my_putchar('\n');
}
