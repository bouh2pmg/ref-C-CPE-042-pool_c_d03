#include <unistd.h>

void alpha()
{
  char c = 'A' - 1;

  while (++c <= 'Z')
    write(1, &c, 1);
  write(1, "\n", 1);
}
