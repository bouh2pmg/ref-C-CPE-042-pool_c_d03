/*
** my_aff_comb2.c for Day 03 in /home/thing-_a/rendu/Piscine-C-Jour_03
** 
** Made by a
** Login   <thing-_a@epitech.net>
** 
** Started on  Wed Oct  2 10:04:48 2013 a
** Last update Wed Oct 28 20:35:29 2015 Camalao
*/

#include <unistd.h>

void my_putchar(char c)
{
  write(1, &c, 1);
}

int	show_number2(int i, int j, int k, int l)
{
  my_putchar(i);
  my_putchar(j);
  my_putchar(' ');
  my_putchar(k);
  my_putchar(l);
  if (i != '9' || j != '8' || k != '9' || l != '9')
    {
      my_putchar(',');
      my_putchar(' ');
    }
  return (0);
}

int	my_aff_comb_second(int k, int l, int j, int i)
{
  k = '0';
  while (k <= '9')
    {
      l = '0';
      while (l <= '9')
	{
	  if ((k * 10) + l > (i * 10) + j)
	    show_number2(i, j, k, l);
	  l = l + 1;
	}
      k = k + 1;
    }
  return (0);
}

int	my_aff_comb2()
{
  int	i;
  int	j;
  int	k;
  int	l;

  i = '0';
  while (i <= '9')
    {
      j = '0';
      while (j <= '9')
	{
	  my_aff_comb_second(k, l, j, i);
	  j = j + 1;
	}
      i = i + 1;
    }
  return (0);
}
