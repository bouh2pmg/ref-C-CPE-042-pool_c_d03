#include <unistd.h>

void	conditions(int n)
{
  char	c;

  if (n == 0)
    c = '0';
  else if (n < 0)
    c = '-';
  else
    c = '+';
  write(1, &c, 1);
}
