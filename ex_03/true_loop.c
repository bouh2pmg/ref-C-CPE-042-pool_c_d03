#include <unistd.h>

void my_true_loop(unsigned int n)
{
  while (n-- > 0)
    write(1, "+", 1);
  write(1, "\n", 1);
}
