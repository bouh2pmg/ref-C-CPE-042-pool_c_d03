/*
** my_aff_comb.c for Day 03 in /home/thing-_a/rendu/Piscine-C-Jour_03
** 
** Made by a
** Login   <thing-_a@epitech.net>
** 
** Started on  Wed Oct  2 09:26:57 2013 a
** Last update Wed Oct 28 20:35:00 2015 Camalao
*/
#include <unistd.h>

void my_putchar(char c)
{
  write(1, &c, 1);
}

int	show_number(int f, int s, int t)
{
  my_putchar(f);
  my_putchar(s);
  my_putchar(t);
  if (f != '7' || s != '8' || t != '9')
    {
      my_putchar(',');
      my_putchar(' ');
    }
  return (0);
}

int	my_aff_comb()
{
  int	i;
  int	j;
  int	k;

  i = '0';
  while (i <= '9')
    {
      j = i + 1;
      while (j <= '9')
	{
	  k = j + 1;
	  while (k <= '9')
	    {
	      show_number(i, j, k);
	      k = k + 1;
	    }
	  j = j + 1;
	}
      i = i + 1;
    }
  return (0);
}
